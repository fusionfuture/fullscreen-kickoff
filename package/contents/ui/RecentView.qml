/*
    SPDX-FileCopyrightText: 2023 Fushan Wen <qydwhotmail@gmail.com>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

import QtQuick
import QtQuick.Controls as QQC2
import QtQuick.Layouts

Item {
    id: recentView

    TapHandler {
        onTapped: window.close()
    }

    Item {
        anchors {
            fill: parent
            leftMargin: goToAppViewButton.width + 50
            rightMargin: goToAppViewButton.width + 50
            margins: window.height / 20
        }

        Item {
            anchors {
                left: parent.left
                top: parent.top
                bottom: parent.bottom
                right: parent.horizontalCenter
                rightMargin: 10
            }

            Text {
                id: recentFilesTitle
                anchors {
                    left: parent.left
                    top: parent.top
                }
                color: "white"
                text: qsTr("Recent Files")
                font.pixelSize: 64
            }

            QQC2.ScrollView {
                id: recentFilesScrollView
                anchors {
                    left: parent.left
                    right: parent.right
                    top: recentFilesTitle.bottom
                    topMargin: 10
                    bottom: parent.bottom
                }

                ListView {
                    id: recentFilesListView
                    flickableDirection: Flickable.VerticalFlick
                    model: 16
                    delegate: Rectangle {
                        width: recentFilesListView.width
                        height: window.height / 10
                        color: Qt.rgba(Math.random(), Math.random(), Math.random(), 1)
                    }
                }
            }
        }

        Item {
            anchors {
                left: parent.horizontalCenter
                leftMargin: 10
                top: parent.top
                bottom: parent.bottom
                right: parent.right
            }

            Text {
                id: recentAppsTitle
                anchors {
                    left: parent.left
                    top: parent.top
                }
                color: "white"
                text: qsTr("Recent Applications")
                font.pixelSize: 64
            }

            QQC2.ScrollView {
                id: recentAoosScrollView
                anchors {
                    left: parent.left
                    right: parent.right
                    top: recentAppsTitle.bottom
                    topMargin: 10
                    bottom: parent.bottom
                }

                ListView {
                    id: recentAppsListView
                    flickableDirection: Flickable.VerticalFlick
                    model: 16
                    delegate: Rectangle {
                        width: recentAppsListView.width
                        height: window.height / 10
                        color: Qt.rgba(Math.random(), Math.random(), Math.random(), 1)
                    }
                }
            }
        }
    }

    QQC2.Button {
        id: goToAppViewButton
        anchors {
            left: parent.left
            verticalCenter: parent.verticalCenter
        }
        text: qsTr("←")

        onClicked: centerView.decrementCurrentIndex()
    }
}

