/*
    SPDX-FileCopyrightText: 2023 Fushan Wen <qydwhotmail@gmail.com>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

import QtQuick

Window {
    id: window

    x: window.screen.virtualX
    y: window.screen.virtualY
    width: window.screen.width
    height: window.screen.height

    flags: Qt.BypassWindowManagerHint | Qt.FramelessWindowHint
    visibility: Window.FullScreen

    color: Qt.rgba(0, 0, 0, 0.9)

    TapHandler {
        onTapped: window.close()
    }

    Item {
        id: content

        anchors {
            fill: parent
            topMargin: window.height / 20
            bottomMargin: window.height / 20
            leftMargin: window.width / 20
            rightMargin: window.width / 20
        }

        Header {
            id: header

            anchors {
                left: parent.left
                right: parent.right
                top: parent.top
            }
            height: Math.min(100, window.height / 20)
        }

        CenterView {
            id: centerView

            anchors {
                left: parent.left
                right: parent.right
                top: header.bottom
                bottom: parent.bottom
            }
            visible: header.searchField.text.length === 0
            focus: true
        }

        SearchView {
            id: searchView
            anchors.fill: centerView
            visible: header.searchField.text.length > 0
        }
    }
}
