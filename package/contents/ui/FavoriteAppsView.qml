/*
    SPDX-FileCopyrightText: 2023 Fushan Wen <qydwhotmail@gmail.com>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

import QtQuick
import QtQuick.Controls as QQC2

Item {
    id: favoriteAppsView

    Grid {
        id: favoriteAppGridView
        anchors.centerIn: parent
        rows: 4
        columns: rows
        rowSpacing: 10
        columnSpacing: rowSpacing

        Repeater {
            model: 16
            Rectangle {
                width: Math.min(window.width, window.height) / 6
                height: width
                color: Qt.rgba(Math.random(), Math.random(), Math.random(), 1)
            }
        }
    }

    QQC2.Button {
        id: goToRecentViewButton
        anchors {
            right: parent.right
            verticalCenter: parent.verticalCenter
        }
        text: qsTr("→")

        onClicked: centerView.incrementCurrentIndex()
    }

    QQC2.Button {
        id: goToAllAppsViewButton
        anchors {
            horizontalCenter: parent.horizontalCenter
            bottom: parent.bottom
        }
        text: qsTr("↓↓↓ All Applications ↓↓↓")

        onClicked: appSwipeView.incrementCurrentIndex()
    }
}
