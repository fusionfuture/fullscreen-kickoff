/*
    SPDX-FileCopyrightText: 2023 Fushan Wen <qydwhotmail@gmail.com>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

import QtQuick
import QtQuick.Controls as QQC2

QQC2.SwipeView {
    id: centerView

    clip: true
    // Do not let user navigate from All apps to Recent apps
    interactive: appSwipeView.currentItem !== appSwipeView.allAppsView
    orientation: Qt.Horizontal

    Keys.forwardTo: header.searchField
    Keys.onEscapePressed: {
        if (header.searchField.text.length > 0) {
            header.searchField.text = "";
        } else {
            window.close();
        }
    }

    AppSwipeView {
        id: appSwipeView
    }

    RecentView {
        id: recentView
    }
}
