/*
    SPDX-FileCopyrightText: 2023 Fushan Wen <qydwhotmail@gmail.com>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

import QtQuick
import QtQuick.Controls as QQC2
import QtQuick.Layouts

// import org.kde.kirigamiaddons.components as KirigamiComponents
// import org.kde.kirigami as Kirigami
// import org.kde.coreaddons as KCoreAddons
// import org.kde.config as KConfig
// import org.kde.kcmutils as KCM
// import org.kde.plasma.components as PC3
// import org.kde.plasma.extras as PlasmaExtras

Item {
    id: header

    readonly property alias searchField: searchField

    // KCoreAddons.KUser {
    //     id: kuser
    // }

    // BEGIN Avatar
    Row {
        id: nameAndIcon
        // spacing: Kirigami.Units.largeSpacing
        anchors {
            left: parent.left
        }
        width: window.width / 7
        height: header.height
        spacing: 10

        // KirigamiComponents.AvatarButton {
        //     id: avatar
        //     visible: KConfig.KAuthorized.authorizeControlModule("kcm_users")

        //     Layout.fillHeight: true
        //     Layout.minimumWidth: height
        //     Layout.maximumWidth: height

        //     text: qsTr("Open user settings")
        //     name: kuser.fullName
        //     source: kuser.faceIconUrl

        //     onClicked: KCM.KCMLauncher.openSystemSettings("kcm_users")
        // }

        // Kirigami.Heading {
        //     id: nameLabel
        //     color: Kirigami.Theme.textColor
        //     level: 4
        //     text: kuser.fullName
        //     elide: Text.ElideRight
        //     horizontalAlignment: Text.AlignLeft
        //     verticalAlignment: Text.AlignVCenter
        // }
        QQC2.Button {
            height: parent.height
            icon.name: "im-user-symbolic"
        }

        Text {
            height: parent.height
            text: "Test User"
            fontSizeMode: Text.VerticalFit
            minimumPixelSize: 10
            font.pixelSize: 72
            color: "white"
        }
    }
    // END Avatar

    // BEGIN Search
    QQC2.TextField {
        id: searchField

        anchors {
            horizontalCenter: header.horizontalCenter
        }
        width: window.width / 2
        height: parent.height
    }
    // END Search

    // BEGIN Power
    QQC2.Button {
        id: powerButton
        anchors {
            right: parent.right
        }
        height: parent.height
        display: QQC2.AbstractButton.IconOnly
        icon.name: "system-shutdown-symbolic"
        text: qsTr("Shutdown")
    }
    //End Power
}
