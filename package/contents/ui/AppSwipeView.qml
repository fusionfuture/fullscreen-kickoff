/*
    SPDX-FileCopyrightText: 2023 Fushan Wen <qydwhotmail@gmail.com>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

import QtQuick
import QtQuick.Controls as QQC2

QQC2.SwipeView {
    id: appSwipeView

    orientation: Qt.Vertical

    readonly property alias allAppsView: allAppsView

    TapHandler {
        parent: appSwipeView.currentItem === favoriteAppsView ? favoriteAppsView : allAppsView.allAppsView.currentItem
        onTapped: window.close()
    }

    FavoriteAppsView {
        id: favoriteAppsView
    }

    AllAppsView {
        id: allAppsView

        QQC2.SwipeView.onIsCurrentItemChanged: if (!QQC2.SwipeView.isCurrentItem) {
            allAppsView.allAppsView.currentIndex = 0;
        }
    }
}
