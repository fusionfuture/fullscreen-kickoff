/*
    SPDX-FileCopyrightText: 2023 Fushan Wen <qydwhotmail@gmail.com>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

import QtQuick

Item {
    id: searchView

    Text {
        anchors.centerIn: parent
        color: "white"
        font.pixelSize: 72
        text: qsTr("Serch results for \"") + header.searchField.text + qsTr("\"")
    }
}
