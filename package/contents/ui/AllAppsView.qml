/*
    SPDX-FileCopyrightText: 2023 Fushan Wen <qydwhotmail@gmail.com>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

import QtQuick
import QtQuick.Controls as QQC2

Item {
    readonly property alias allAppsView: allAppsView
    QQC2.SwipeView {
        id: allAppsView
        anchors {
            left: parent.left
            right: parent.right
            top: parent.top
            bottom: pageIndicator.top
        }

        orientation: Qt.Horizontal

        Repeater {
            model: 4

            Item {
                readonly property int rootIndex: modelData
                Grid {
                    id: favoriteAppGridView
                    anchors.centerIn: parent
                    rows: 5
                    columns: 15
                    rowSpacing: 10
                    columnSpacing: rowSpacing

                    Repeater {
                        model: 75
                        QQC2.Button {
                            width: window.width / 20
                            height: window.height / 8

                            display: QQC2.AbstractButton.TextUnderIcon
                            icon.name: "application-octet-stream"
                            text: `${rootIndex} - ${index}`
                        }
                    }
                }
            }
        }
    }

    QQC2.Button {
        id: goToFavAppsViewButton
        anchors {
            left: parent.left
            verticalCenter: pageIndicator.verticalCenter
        }
        text: qsTr("↑↑↑ Favorite Applications ↑↑↑")

        onClicked: appSwipeView.decrementCurrentIndex()
    }

    QQC2.PageIndicator {
        id: pageIndicator
        anchors {
            horizontalCenter: parent.horizontalCenter
            bottom: parent.bottom
        }
        currentIndex: allAppsView.currentIndex
        count: allAppsView.count
        interactive: true
        delegate: Item {
            width: 30 + 20
            height: 30

            property bool isPressed: pressed

            onIsPressedChanged: if (isPressed) {
                allAppsView.currentIndex = index;
            }

            Rectangle {
                anchors.horizontalCenter: parent.horizontalCenter
                width: 30
                height: 30
                color: index === pageIndicator.currentIndex ? "white" : "gray"
                radius: 30
            }
        }
    }
}

