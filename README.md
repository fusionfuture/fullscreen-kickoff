# Fullscreen Kickoff

## Description
See https://discuss.kde.org/t/fullscreen-kickoff/4281

## Visuals (Prototype)
|                    Favorite Applications                     |                     All Applications                     |                Recent                |
| :----------------------------------------------------------: | :------------------------------------------------------: | :----------------------------------: |
| ![Favorite Applications](./screenshots/Favorite Applications.webp) | ![All Applications](./screenshots/All Applications.webp) | ![Recent](./screenshots/Recent.webp) |



## Installation
TODO

## Usage

To test the prototype, just run:

```shell
qml6 package/contents/ui/DashboardRepresentation.qml
```

## Roadmap
- [ ] Add models
- [ ] Replace placeholders
- [ ] Nav button icons
- [ ] Reuse components from Kickoff

## License
GPLv2+
